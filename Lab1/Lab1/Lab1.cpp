﻿#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>

using namespace std;

/*---------------------------------------------------------------*/

void VectorRendering(vector<int>* arr)
{
    for (int el : *arr) cout << el << " ";
    cout << endl;
}

/*---------------------------------------------------------------*/

void VectorRendering(int* arr, int length)
{
    if (arr != nullptr)
    {
        for (int i = 0; i < length; i++) cout << arr[i] << "  ";
        cout << endl;
    }
}

/*---------------------------------------------------------------*/

//Render matrix on the screen. nrow - rows count; ncol - colums count
void MatrixRendering(int** mat, int nrow, int ncol)
{
    if (mat != nullptr)
    {
        for (int i = 0; i < nrow; i++) VectorRendering(mat[i], ncol);
    }
}

/*---------------------------------------------------------------*/

vector<int>* GetMaxRVectorFromMatrix(int** mat, int length)
{
    vector<int>* maxR = new vector<int>();

    //Default maxRX
    for (int i = 0; i < length; i++) maxR->push_back(1);

    //Determine maxR element
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
           if (mat[i][j] == 1 && mat[j][i] == 0) (*maxR)[j] = 0;
        }
    }

    //Exclude the equal elements
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            if (mat[i][j] == 1 && mat[j][i] == 1 && (*maxR)[i] == 0) 
                (*maxR)[j] = 0;
        }
    }

    return maxR;
}



int main()
{
    int** mat = new int* [5]
    {
        new int[5] { 0, 1, 0, 1, 0 },
        new int[5] { 1, 0, 1, 0, 0 },
        new int[5] { 0, 0, 0, 1, 1 },
        new int[5] { 0, 0, 1, 0, 0 },
        new int[5] { 0, 0, 0, 0, 0 }
    };


    MatrixRendering(mat, 5, 5);
    vector<int> *result = GetMaxRVectorFromMatrix(mat, 5);
    
    cout << "Your result is " << endl;
    VectorRendering(result);

}
