#include "Matrix.h"

/*---------------------------------------------------------*/

Matrix::Matrix(int** mat, int size)
{
	content = GetMatrixCopy(mat, size);
	resolution = size;
}

/*---------------------------------------------------------*/

Matrix::Matrix(Matrix& obj)
{
	resolution = obj.resolution;
	content = GetMatrixCopy(obj.content, resolution);
}

/*---------------------------------------------------------*/


int** Matrix::GetMatrixCopy(int** mat, int size)
{

	int** res = new int* [size];

	for (int i = 0; i < size; i++)
	{
		res[i] = new int[size];

		for (int j = 0; j < size; j++) res[i][j] = mat[i][j];
	}

	return res;
}