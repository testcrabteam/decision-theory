#include "Additive.h"
#include <iostream>

using namespace std;

void VectorRendering(vector<int>* arr)
{
    if (arr && !arr->empty())
    {
        for (int el : *arr) cout << el << " ";
    }       
    else cout << "Empty!";
    cout << endl;
}

void VectorRendering(vector<float>* arr)
{
    for (float el : *arr) cout << el << " ";
    cout << endl;
}

void VectorRendering(int* arr, int length)
{
    if (arr != nullptr)
    {
        for (int i = 0; i < length; i++) cout << arr[i] << "  ";
        cout << endl;
    }
}


//Render matrix on the screen. nrow - rows count; ncol - colums count
void MatrixRendering(int** mat, int nrow, int ncol)
{
    if (mat != nullptr)
    {
        for (int i = 0; i < nrow; i++) VectorRendering(mat[i], ncol);
    }
}
