﻿#include <iostream>
#include "Utility.h"
#include "Additive.h"

using namespace std;

int main()
{
    int size = 7;
    int** mat = new int* [size]
    {
        new int[size] { 0, 0, 0, 0, 0, 0, 0 },
        new int[size] { 0, 0, 1, 0, 0, 0, 0 },
        new int[size] { 0, 0, 0, 1, 0, 0, 1 },
        new int[size] { 0, 1, 0, 0, 1, 1, 0 },
        new int[size] { 0, 1, 0, 0, 0, 0, 0 },
        new int[size] { 1, 0, 0, 1, 0, 0, 0 },
        new int[size] { 0, 0, 1, 0, 0, 1, 0 }
    };

    cout << "Your can work with matrix below!" << endl;
    MatrixRendering(mat, size, size);
    cout << endl;

    System* system = GetMaxSolutionsVector(mat, size);
    vector<int>* solutions = system->solutions;
    vector<float>* utilities = system->statement->utilities;

    cout << "Your utilities are below!" << endl;
    VectorRendering(utilities);
    cout << endl;

    cout << "Your max solutions vector are below!" << endl;
    VectorRendering(solutions);

}
