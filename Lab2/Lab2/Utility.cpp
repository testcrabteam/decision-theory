#include <iostream>
#include <cmath>
#include <algorithm>
#include "Utility.h"
#include "Additive.h"

/*---------------------------------------------------------*/

System* GetMaxSolutionsVector(int** mat, int size)
{
	SystemStatement* statement = SimulateSystem(mat, size);
	vector<float>* utilities = statement->utilities;
	vector<int>* alternatives = statement->alternatives;

	vector<int>* res = new vector<int>();
	float maxUtility = GetMax(utilities);

	for (int i = 0; i < utilities->size(); i++)
	{
		float el = utilities->at(i);

		if (el == maxUtility) 
			res->push_back(alternatives->at(i));
	}

	System* system = new System();
	system->solutions = res;
	system->statement = statement;

	return system;

}

/*---------------------------------------------------------*/

SystemStatement* SimulateSystem(int** mat, int size)
{
	Matrix* matrix = new Matrix(mat, size);
	SystemStatement* statement = new SystemStatement(matrix);
	
	vector<int>* alt = statement->alternatives;
	vector<int>* usedElements = new vector<int>({0});
	vector<int>* much;
	vector<int>* less;
	float currentUtility;
	int currentElement = 0;
	int prevEl;
	bool isUsed;

	for (int i = 0; i < size; i++)
	{
		if (i != 0)
		{
			cout << "N + 1 = " << currentElement << endl;

			much = GetMuchSolutions(statement, currentElement);
			less = GetLessSolutions(statement, currentElement);

			cout << "Much solutions: ";
			VectorRendering(much);

			cout << "Less solutions: ";
			VectorRendering(less);

			currentUtility = GetUtilityFunction(statement, much, less);
			cout << "Current utility function: " << currentUtility << endl << endl;

			//Save our utility
			(*statement->utilities)[currentElement] = currentUtility;

		}
		else
		{
			cout << "First iteration!" << endl;
			cout << "N + 1 = " << 0 << endl;
			cout << "Current utility function: " << 0 << endl << endl;
		}

		//Find element for next iteration
		prevEl = currentElement;
		for (int j = 0; j < size; j++)
		{
			isUsed = std::count(usedElements->begin(), usedElements->end(), j);

			if (!isUsed && (matrix->content[j][currentElement] == 1 || matrix->content[currentElement][j] == 1))
			{
				currentElement = j;
				break;
			}
		}
		usedElements->push_back(i);

		if (currentElement == prevEl)
		{
			for (int j = 0; j < size; j++)
			{
				isUsed = std::count(usedElements->begin(), usedElements->end(), j);

				if (!isUsed)
				{
					currentElement = j;
					break;
				}
			}
		}
	}

	return statement;
}

/*---------------------------------------------------------*/

vector<int>* GetMuchSolutions(SystemStatement* statement, int target)
{
	Matrix* mat = new Matrix(*statement->systemMatrix);
	vector<int>* alt = new vector<int>(*statement->alternatives);
	int size = mat->resolution;

	vector<int>* res = new vector<int>();

	for (int i = 0; i < target; i++)
	{
		if (mat->content[i][target] == 1)
			res->push_back(i);
	}

	return res;
}

/*---------------------------------------------------------*/

vector<int>* GetLessSolutions(SystemStatement* statement, int target)
{
	Matrix* mat = new Matrix(*statement->systemMatrix);
	vector<int>* alt = new vector<int>(*statement->alternatives);
	int size = mat->resolution;

	vector<int>* res = new vector<int>();

	for (int i = 0; i < target; i++)
	{
		if (mat->content[target][i] == 1)
			res->push_back(i);
	}

	return res;
}

/*---------------------------------------------------------*/

float GetUtilityFunction(SystemStatement* statement, vector<int>* much, vector<int>* less)
{
	if (much->empty())
	{
		int x1 = GetMax(less);
		float u1 = statement->utilities->at(x1);
		return u1 + 1;
	}
	if (less->empty())
	{
		int x2 = GetMin(much);
		float u2 = statement->utilities->at(x2);
		return u2 - 1;
	}

	//It isn't empty sets

	int x1 = GetMax(less);
	int x2 = GetMin(much);

	float u1 = statement->utilities->at(x1);
	float u2 = statement->utilities->at(x2);

	vector<int>* cross = GetCrossElements(much, less);

	if (cross->empty()) return ((u1 + u2) / 2);
	else return statement->utilities->at(cross->at(0));

}

/*---------------------------------------------------------*/

/*Additive functions*/

/*---------------------------------------------------------*/

int GetMax(vector<int>* v)
{
	int max = v->at(0);

	for (int el : *v)
	{
		if (el > max && el != 0) 
			max = el;
	}

	return max;
}

/*---------------------------------------------------------*/

float GetMax(vector<float>* v)
{
	float max = floor(v->at(0) * 100) / 100;
	float roundEl;

	for (float el : *v)
	{
		roundEl = floor(el * 100) / 100;

		if (el > max)
			max = el;
	}

	return max;
}

/*---------------------------------------------------------*/

int GetMin(vector<int>* v)
{
	int min = v->at(0);

	for (int el : *v)
	{
		if (el < min && el != 0)
			min = el;
	}

	return min;
}

/*---------------------------------------------------------*/

vector<int>* GetCrossElements(vector<int>* v1, vector<int>* v2)
{
	vector<int>* res = new vector<int>();

	for (int el1 : *v1)
		for (int el2 : *v2)
			if (el1 == el2) res->push_back(el1);
	return res;
}


