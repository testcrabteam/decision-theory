#pragma once


class Matrix
{

public:

	int** content;
	int resolution;			//Size

	Matrix(int** mat, int size);
	Matrix(Matrix& obj);

private:

	int** GetMatrixCopy(int** mat, int size);

};