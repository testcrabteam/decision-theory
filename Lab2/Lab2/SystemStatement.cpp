#include "SystemStatement.h"

/*---------------------------------------------------------*/

SystemStatement::SystemStatement(Matrix* matrix)
{
	systemMatrix = new Matrix(*matrix);

	alternatives = new vector<int>();
	for (int i = 0; i < systemMatrix->resolution; i++) alternatives->push_back(i);	//1, 2, 3...

	utilities = new vector<float>(systemMatrix->resolution, 0);
}

/*---------------------------------------------------------*/

SystemStatement::SystemStatement(SystemStatement& obj)
{
	systemMatrix = new Matrix(*obj.systemMatrix);
	alternatives = new vector<int>(*obj.alternatives);
	utilities = new vector<float>(*obj.utilities);

}