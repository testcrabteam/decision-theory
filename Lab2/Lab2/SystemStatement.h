#pragma once
#include <vector>
#include "Matrix.h"

using namespace std;

class SystemStatement
{
public:
	Matrix* systemMatrix;
	vector<int>* alternatives;
	vector<float>* utilities;

	//Default system statement - all elements are in alternative
	SystemStatement(Matrix* matrix);

	SystemStatement(SystemStatement& obj);
};

