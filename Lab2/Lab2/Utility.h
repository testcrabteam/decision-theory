#pragma once
#include "SystemStatement.h"

struct System
{
	SystemStatement* statement;
	vector<int>* solutions;
};

//Get result effective solutions
System* GetMaxSolutionsVector(int** mat, int size);

//Create system statement with ready utilities and alternatives
SystemStatement* SimulateSystem(int** mat, int size);

//Get solutions, mush target
vector<int>* GetMuchSolutions(SystemStatement* statement, int target);

//Get solutions, less target
vector<int>* GetLessSolutions(SystemStatement* statement, int target);

//Calc utility func with much and less solutions
float GetUtilityFunction(SystemStatement* statement, vector<int>* much, vector<int>* less);


/*---------------------------------------------------------*/

/*Additive functions*/

/*---------------------------------------------------------*/

int GetMax(vector<int>* v);

/*---------------------------------------------------------*/

float GetMax(vector<float>* v);

/*---------------------------------------------------------*/

int GetMin(vector<int>* v);

/*---------------------------------------------------------*/

vector<int>* GetCrossElements(vector<int>* v1, vector<int>* v2);