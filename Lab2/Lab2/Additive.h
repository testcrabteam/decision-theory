#pragma once
#include <vector>

using namespace std;

void VectorRendering(vector<int>* arr);

void VectorRendering(vector<float>* arr);

void VectorRendering(int* arr, int length);

//Render matrix on the screen. nrow - rows count; ncol - colums count
void MatrixRendering(int** mat, int nrow, int ncol);
